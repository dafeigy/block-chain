import numpy as math
def cal_possibility(z,q):
    p = 1 - q
    sum = 1
    lambda_ = z * ( q / p )
    for k in range(z+1):
        expont=math.exp(-1*lambda_)*math.power(lambda_,k)/math.math.factorial(k)
        sum -= expont * (1 - math.power(q/p,z-k))
    return sum
for z in range(1,501,20):
    for q in range (0,51,2):
        print("等待时间为"+str(z)+'，攻击成功概率为'+str(q/100)+"时的突破概率"+str(cal_possibility(z,q/100)))
        if q == 50:
            print(str(z)+' is finshed')
